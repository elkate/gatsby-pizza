const nodemailer = require('nodemailer');

function generateOrderEmail({order, total}) {
  return `<div>
    <h2>Your Recenet Order for ${total}</h2> 
    <p>Please start walking over, we will have your order ready in the next 20 mins.</p>
    <ul>
      ${order.map(item => `<li>
      <img src="${item.thumbnail}" alt="${item.name}"/>
      ${item.size} ${item.name} * ${item.count} - ${item.price}
      </li>`).join('')}
    </ul>
    <p> Your Total is ${total} due at pickup</p>
    <style>
      ul {
        list-style: none;
      }
    </style>
  </div>`;
}


const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: 587,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD
  }
});

async function wait(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms);
  });
}

exports.handler = async (event, context) => {
  await wait(1000);
  const body = JSON.parse(event.body);
  const requiredFields = ['email', 'name', 'order'];

  if (body.mapleSyrup) {
    return {
      statusCode: 400,
      body: JSON.stringify({message: 'Err'})
    };
  }

  for (const field of requiredFields) {
    if (!body[field]) {
      return {
        statusCode: 400,
        body: JSON.stringify({message: `You are missing the ${field} field`})
      };
    }
  }

  if (!body.order.length) {
    return {
      statusCode: 400,
      body: JSON.stringify({message: 'Empty order'})
    };
  }

  const info = await transporter.sendMail({
    from: 'Slicks Slices <slick@slicktest.com>',
    to: `${body.name} <${body.email}>, katerina17778@mail.ru`,
    subject: 'New order!',
    html: generateOrderEmail({order: body.order, total: body.total}), 
  });

  return {
    statusCode: 200,
    body: JSON.stringify({message: 'Success!'})
  };
};
