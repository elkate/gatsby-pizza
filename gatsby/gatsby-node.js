import path from 'path';
import fetch from 'isomorphic-fetch';

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  if (stage.startsWith('develop')) {
    actions.setWebpackConfig({
      resolve: {
        alias: {
          'react-dom': '@hot-loader/react-dom',
        },
      },
    });
  }
};

async function turnPizzasIntoPages({ graphql, actions }) {
  const pizzaTemplate = path.resolve('./src/templates/Pizza.js');

  const { data } = await graphql(`
    query {
      pizzas: allSanityPizza {
        nodes {
          name,
          slug {
            current
          }
        }
      }
    }
  `);

  data.pizzas.nodes.forEach(pizza => {
    actions.createPage({
      path: `pizza/${pizza.slug.current}`,
      component: pizzaTemplate,
      context: {
        slug: pizza.slug.current
      }
    });
  });
}

async function turnToppingsIntoPages({ graphql, actions }) {
  const toppingTemplate = path.resolve('./src/pages/pizzas.js');

  const { data } = await graphql(`
  query {
    toppings: allSanityTopping {
      nodes {
        name
        id
        slug {
          current
        }
      }
    }
  }
`);

  data.toppings.nodes.forEach(topping => {
    actions.createPage({
      path: `topping/${topping.slug.current}`,
      component: toppingTemplate,
      context: {
        toppingId: topping.id
      }
    });
  });
}

async function fetchBeersAndTurnIntoNodes({
  actions, 
  createNodeId,
  createContentDigest
}) {
  const res = await fetch('https://api.punkapi.com/v2/beers');
  const beers = await res.json();

  for (const beer of beers) {
    const nodeMeta = {
      id: createNodeId(`beer-${beer.name}`),
      parent: null,
      children: [],
      internal: {
        type: 'Beer',
        mediaType: 'application/json',
        contentDigest: createContentDigest(beer),
      }
    };
    actions.createNode({
      ...beer,
      ...nodeMeta,
    });
  }
}

async function turnSlicemastersIntoPages({ graphql, actions }) {
  const slicemastersTemplate = path.resolve('./src/pages/slicemasters.js');

  const { data } = await graphql(`
    query {
      slicemasters: allSanityPerson {
        totalCount
        nodes {
          name
          id
          slug {
              current
          }
        }
      }
    }`);

  const pageSize = parseInt(process.env.GATSBY_PAGE_SIZE);
  const pageCount = Math.ceil(data.slicemasters.totalCount / pageSize);

  Array.from({length: pageCount}).forEach((_, i) => {
    actions.createPage({
      path: `/slicemasters/${i + 1}`,
      component: slicemastersTemplate,
      context: {
        skip: i * pageSize,
        currentPage: i + 1,
        pageSize,
      }
    });
  });
}

async function turnSlicemasterIntoPages({ graphql, actions }) {
  const personTemplate = path.resolve('./src/templates/Slicemaster.js');

  const { data } = await graphql(`
    query {
      slicemasters: allSanityPerson {
        nodes {
          name
          id
          slug {
              current
          }
        }
      }
    }
  `);

  data.slicemasters.nodes.forEach(person => {
    actions.createPage({
      path: `slicemaster/${person.slug.current}`,
      component: personTemplate,
      context: {
        slug: person.slug.current
      }
    });
  });
}


export async function sourceNodes(params) {
  await Promise.all([fetchBeersAndTurnIntoNodes(params)]);
}

export async function createPages(params) {
  await Promise.all([
    turnPizzasIntoPages(params),
    turnToppingsIntoPages(params),
    turnSlicemastersIntoPages(params),
    turnSlicemasterIntoPages(params)
  ]);
}