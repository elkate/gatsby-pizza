import React from 'react';
import Img from 'gatsby-image';
import MenuItemStyles from '../styles/MenuItemStyles';
import calculatePizzaPrice from '../utils/calculatePizzaPrice';
import formatMoney from '../utils/formatMoney';

export default function PizzaOrder({order, pizzas, removeFromOrder}) {
  return <>
    {order.map((singleOrder, key) => {
      const pizza = pizzas.find(pizza => pizza.id === singleOrder.id);

      return <MenuItemStyles key={key}>
        <Img fluid={pizza.image.asset.fluid} />
        <h2>{pizza.name} ({singleOrder.size})</h2>
        <p>{formatMoney(calculatePizzaPrice(pizza.price, singleOrder.size))} 
          {singleOrder.count > 1 ? ` * ${singleOrder.count} = ${formatMoney(calculatePizzaPrice(pizza.price * singleOrder.count, singleOrder.size))}` : ''} 
        </p>
        <button 
          type="button" 
          className="remove"
          title={`Remove ${singleOrder.size} ${pizza.name} from Order`}
          onClick={() => removeFromOrder(key)}
        >&times;</button> 
      </MenuItemStyles>;
    })}
  </>;
}