import React from 'react';
import useLatestData from '../utils/useLatestData';
import {HomePageGrid} from '../styles/Grids';
import LoadingGrid from '../components/LoadingGrid';
import ItemGrid from '../components/ItemGrid';

function CurrentlySlicing({slicemasters}) {
  return <div>
    <h2 className="center">
      <span className="mark tilt">Slicemasters</span>
    </h2>
    <p></p>
    {!slicemasters && <LoadingGrid count={4}/>}
    {slicemasters && !slicemasters?.length &&
      <p>No one is working right now</p>
    }
    {slicemasters?.length && <ItemGrid items={slicemasters} />}
  </div>;
}

function HotSlices({hotSlices}) {
  return <div>
    <h2 className="center">
      <span className="mark tilt">Hot Slices</span>
    </h2>
    <p></p>
    {!hotSlices && <LoadingGrid count={4}/>}
    {hotSlices && !hotSlices?.length &&
      <p>Nothing</p>
    }
    {hotSlices?.length && <ItemGrid items={hotSlices} />}
  </div>;
}

export default function HomePage() {
  const {sliceMasters, hotSlices} = useLatestData();

  return <div className="center">
    <h1>THe Best Pizza</h1>
    <HomePageGrid>
      <CurrentlySlicing slicemasters={sliceMasters} />
      <HotSlices hotSlices={hotSlices}/>
    </HomePageGrid>
    <p></p>
  </div>;
}
