import { graphql, Link } from 'gatsby';
import React from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';
import Pagination from '../components/Pagination';
import Seo from '../components/Seo';

const SliceMasterGrid = styled.div`
    display: grid;
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
`;

const SliceMasterStyles = styled.div`
    a {
        text-decoration: none;
    }

    .gatsby-image-wrapper {
        height: 400px;
        object-fit: contain;
    }

    h2 {
        transform: rotate(-2deg);
        transition: transform 0.2s ease-in-out;
        text-align: center;
        position: relative;
        margin-bottom: -2rem;
        font-size: 4rem;
        z-index: 2;

        &:hover {
            transform: rotate(2deg);
        }
    }

    .description {
        background-color: var(--yellow);
        margin: 2rem;
        margin-top: -6rem;
        position: relative;
        z-index: 2;
        padding: 1rem;
        transform: rotate(1deg);
        text-align: center;
    }
`;

export default function SlicemastersPage({ data, pageContext }) {
  const slicemasters = data.slicemasters.nodes;

  return <>
    <Seo title={`Slicemasters - Page ${pageContext.currentPage || 1}`} />

    <Pagination 
      pageSize={parseInt(process.env.GATSBY_PAGE_SIZE)} 
      totalCount={data.slicemasters.totalCount}
      currentPage={pageContext.currentPage || 1}
      skip={pageContext.skip}
      base="/slicemasters"/>
    <SliceMasterGrid>
      {slicemasters.map(person => (
        <SliceMasterStyles key={person.id}>
          <Link to={`/slicemaster/${person.slug.current}`}>
            <h2>
              <span className="mark">{person.name}</span>
            </h2>
          </Link>
          <Img fluid={person.image.asset.fluid}></Img>
          <p className="description">{person.description}</p>
        </SliceMasterStyles>
      ))}
    </SliceMasterGrid>
  </>
  ;
}

export const query = graphql`
    query(
        $skip: Int = 0, 
        $pageSize: Int = 4) {
        slicemasters: allSanityPerson(limit: $pageSize, skip: $skip) {
            totalCount
            nodes {
                name
                id
                slug {
                    current
                }
                description
                image {
                    asset {
                        fluid(maxWidth: 410) {
                            ...GatsbySanityImageFluid
                        }
                    }
                }
            }
        }
    }
`;
