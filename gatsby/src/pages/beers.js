import { graphql } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import Seo from '../components/Seo';

const BeerGridStyles = styled.div`
    display: grid;
    gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
`;

const SingleBeerStyles = styled.div`
    border: 1px solid var(--grey);
    text-align: center;
    padding: 2rem;

    h3 {
      margin: 0.5rem 0;
    }

    span {
      font-size: 1.5rem;
    }

    img {
        width: 100%;
        height: 200px;
        object-fit: contain;
        display: grid;
        align-items: center;
        font-size: 10px;
        color: black;
    }
`;

export default function BeersPage({ data }) {
  return <>
    <Seo title={'Beers!'} />
    <h2 className="center">
        We have {data.beers.nodes.length} Beers Available. Dine in Only!
    </h2>
    <BeerGridStyles>
      {data.beers.nodes.map((beer) => {
        // const rating = Math.round(beer.rating.average);

        return <SingleBeerStyles key={beer.id}>
          <img src={beer.image_url} alt={beer.name} />
          <h3>{beer.name}</h3>
          <span>{beer.tagline}</span>
          
          {/* <p title={`${rating} out of 5 stars`}>
            {'⭐'.repeat(rating)}
            <span style={{filter: 'grayscale(100%)'}}>
              {'⭐'.repeat(5 - rating)}
            </span>
            <span>({beer.rating.reviews})</span>
          </p> */}
        </SingleBeerStyles>;
      })}
    </BeerGridStyles>
  </>;
}

export const query = graphql`
    query {
        beers: allBeer {
          nodes {
            name
            tagline
            image_url
            id
            description
          }
        }
      }`;