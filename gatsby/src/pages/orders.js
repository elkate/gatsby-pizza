import React from 'react';
import Seo from '../components/Seo';
import useForm from '../utils/useForm';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import calculatePizzaPrice from '../utils/calculatePizzaPrice';
import formatMoney from '../utils/formatMoney';
import calculateOrderTotal from '../utils/calculateOrderTotal';
import OrderStyles from '../styles/OrderStyles';
import MenuItemStyles from '../styles/MenuItemStyles';
import usePizza from '../utils/usePizza';
import PizzaOrder from '../components/PizzaOrder';

export default function OrdersPage({data}) {
  const pizzas = data.pizzas.nodes;

  const {values, updateValue} = useForm({
    name: '',
    email: '',
    mapleSyrup: ''
  });

  const {
    order, 
    addToOrder, 
    removeFromOrder,
    error,
    loading,
    message,
    submitOrder
  } = usePizza({
    pizzas,
    values: values
  });

  if (message) {
    return <p>{message}</p>;
  }

  return <>
    <Seo title={'Order a Pizza!'} />
    <OrderStyles onSubmit={submitOrder}>
      <fieldset disabled={loading}>
        <legend>Your Info</legend>
        <label htmlFor="name">Name</label>
        <input 
          type="text" 
          name="name"
          id="name"
          value={values.name} 
          onChange={updateValue}
        />
        <label htmlFor="email">Email</label>
        <input 
          type="text"
          id="email"
          name="email" 
          value={values.email}
          onChange={updateValue}
        />
        <input 
          type="mapleSyrup"
          id="mapleSyrup"
          name="mapleSyrup" 
          value={values.mapleSyrup}
          onChange={updateValue}
          className="hidden"
        />
      </fieldset>
      <fieldset disabled={loading} className="menu">
        <legend>Menu</legend>
        {pizzas.map(pizza => (<MenuItemStyles key={pizza.id}>
          <Img fluid={pizza.image.asset.fluid}  height="50" alt={pizza.name}/>
          <div>
            <h2>{pizza.name}</h2>
          </div>
          <div>
            {['S', 'M', 'L'].map((size, key) => (
              <button 
                key={key} 
                type="button"
                onClick={() => addToOrder({
                  id: pizza.id,
                  size
                })}>
                <span>{size}</span>
                <span>{formatMoney(calculatePizzaPrice(pizza.price, size))}</span>
              </button>))}
          </div>
        </MenuItemStyles>))}
      </fieldset>
      <fieldset disabled={loading} className="order">
        <legend>Order</legend>
        <PizzaOrder 
          order={order} 
          removeFromOrder={removeFromOrder}
          pizzas={pizzas}
        />
      </fieldset>
      <fieldset disabled={loading}>
        <h3>Your Total is {formatMoney(calculateOrderTotal(order, pizzas))}</h3>
        <div>{error ? <p>Error: {error}</p> : '' }</div>
        <button 
          type="submit" 
          disabled={loading}>
          {loading ? 'Placing Order...' : 'Order Ahead'}
        </button>
      </fieldset>
    </OrderStyles>
  </>;
}

export const query = graphql`
  query {
    pizzas: allSanityPizza {
      nodes {
        name,
        id,
        slug { current }
        price
        image {
          asset {
            fluid(maxWidth: 100) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
  }
`;