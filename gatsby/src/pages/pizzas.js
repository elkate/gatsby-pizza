import { graphql } from 'gatsby';
import React from 'react';
import PizzaList from '../components/PizzaList';
import ToppingsFilter from '../components/ToppingsFilter';
import Seo from '../components/Seo';

function getTopingNameById(toppings, toppingId) {
  return toppings.find(topping => topping.id === toppingId).name;
}

export default function PizzasPage({ data, pageContext }) {
  const pizzas = data.pizzas.nodes;
  return <>
    <Seo title={pageContext.toppingId ? `Pizzas With ${getTopingNameById(data.toppings.nodes, pageContext.toppingId)}` : 'All Pizzas'} />
    <ToppingsFilter activeTopping={pageContext.toppingId} />
    <PizzaList pizzas={pizzas} />
  </>;

}

export const query = graphql`
    query PizzaQuery($toppingId: String) {
        toppings: allSanityTopping {
            nodes {
                name
                id
            }
        }
        pizzas: allSanityPizza(
            filter: {
                toppings: {
                    elemMatch: {
                        id: {
                            eq: $toppingId
                        }
                    }
                }
            }
        ) {
            nodes {
                name
                id
                slug {
                    current
                }
                toppings {
                    id
                    name
                }
                image {
                    asset {
                        fixed(width: 200, height: 200) {
                            ...GatsbySanityImageFixed
                        }
                        fluid(maxWidth: 400) {
                            ...GatsbySanityImageFluid
                        }
                    }
                }
            }
        }
    }
`;