import calculatePizzaPrice from '../utils/calculatePizzaPrice';
import formatMoney from '../utils/formatMoney';

export default function attachNamesAndPrices(order, pizzas) {
  return order.map(item => {
    const pizza = pizzas.find(pizza => item.id === pizza.id);

    return {
      ...item,
      name: pizza.name,
      thumbnail: pizza.image.asset.fluid.src,
      price: formatMoney(calculatePizzaPrice(pizza.price * item.count, item.size))
    };
  });
}