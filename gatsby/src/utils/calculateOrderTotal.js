import calculatePizzaPrice from '../utils/calculatePizzaPrice';

export default function calculateOrderTotal(order, pizzas) {
  return order.reduce((sum, orderItem) => {
    const pizza = pizzas.find(pizza => pizza.id === orderItem.id);
    return sum + calculatePizzaPrice(pizza.price * orderItem.count, orderItem.size);
  }, 0);
}