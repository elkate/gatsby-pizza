import {useContext, useState} from 'react';
import OrderContext from '../components/OrderContext';
import calculateOrderTotal from './calculateOrderTotal';
import attachNamesAndPrices from './attachNamesAndPrices';
import formatMoney from './formatMoney';

export default function usePizza({pizzas, values}) {
  const [order, setOrder] = useContext(OrderContext);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState(false);

  function addToOrder(orderedPizza) {
    const index = order.findIndex(orderItem => orderedPizza.id === orderItem.id &&
      orderedPizza.size === orderItem.size);

    if (index !== -1) {
      order[index].count += 1;
      setOrder([
        ...order.slice(0, index),
        order[index],
        ...order.slice(index + 1)
      ]);
    } else {
      orderedPizza.count = 1;
      setOrder([...order, orderedPizza]);
    }
  }

  function removeFromOrder(index) {
    setOrder([
      ...order.slice(0, index),
      ...order.slice(index + 1)
    ]);
  }


  async function submitOrder(e) {
    e.preventDefault();
    setLoading(true);
    setError(null);
    setMessage(null);

    const body = {
      order: attachNamesAndPrices(order, pizzas),
      total: formatMoney(calculateOrderTotal(order, pizzas)),
      name: values.name,
      email: values.email,
      mapleSyrup: values.mapleSyrup
    };

    const res = await fetch(`${process.env.GATSBY_SERVERLESS_BASE}/placeOrder`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body),
    });

    const text = JSON.parse(await res.text());

    if (res.status >= 400 && res.status < 600) {
      setLoading(false);
      setError(text.message);
    } else {
      setLoading(false);
      setMessage('Success!');
    }
  }

  return { order, addToOrder, removeFromOrder, error, loading, message, submitOrder };
}